package overlay

import (
	"os"
	"testing"
)

func TestOverlay(t *testing.T) {
	base := os.DirFS("testdata/overlay")
	ovr := New(base)
	a := os.DirFS("testdata/a")
	b := os.DirFS("testdata/b")
	c := os.DirFS("testdata/c")
	td := os.DirFS("testdata")
	if err := ovr.Mount("td", Mount{Path: "allTestData", FS: td}); err != nil {
		t.Fatalf("Error mounting allTestData: %v", err)
	}
	if err := ovr.Mount("subs",
		Mount{Path: "foo", FS: a},
		Mount{Path: "bar", FS: b},
		Mount{Path: "baz", FS: c}); err != nil {
		t.Fatalf("Error mounting subs: %v", err)
	}
	if err := ovr.Mount("links",
		Mount{Path: "a", Link: "foo"},
		Mount{Path: "b", Link: "bar"},
		Mount{Path: "c", Link: "baz"}); err != nil {
		t.Fatalf("Error mounting links: %v", err)
	}
	t.Log("All filesystems mounted")
	testMountProvides := []Mount{
		{FS: base, Link: ".", Path: "."},
		{FS: base, Link: "real", Path: "real"},
		{FS: a, Link: "foo", Path: "."},
		{FS: a, Link: "foo/foo", Path: "foo"},
		{FS: a, Link: "a", Path: "."},
		{FS: a, Link: "a/foo", Path: "foo"},
		{FS: b, Link: "bar", Path: "."},
		{FS: b, Link: "bar/bar", Path: "bar"},
		{FS: b, Link: "b", Path: "."},
		{FS: b, Link: "b/bar", Path: "bar"},
		{FS: c, Link: "baz", Path: "."},
		{FS: c, Link: "baz/baz", Path: "baz"},
		{FS: c, Link: "c", Path: "."},
		{FS: c, Link: "c/baz", Path: "baz"},
		{FS: td, Link: "allTestData/a/foo", Path: "a/foo"},
		{FS: td, Link: "allTestData/b/bar", Path: "b/bar"},
		{FS: td, Link: "allTestData/c/baz", Path: "c/baz"},
		{FS: td, Link: "allTestData/overlay/real", Path: "overlay/real"},
	}
	for _, mp := range testMountProvides {
		t.Logf("Testing %s", mp.Link)
		tfs, name := ovr.Provides(mp.Link)
		if tfs != mp.FS || name != mp.Path {
			t.Errorf("Unexpected results: mount='%v', path='%v'", tfs, name)
		}
		if fi, err := tfs.Open(name); err != nil {
			t.Errorf("Error opening %s: %v", mp.Link, err)
		} else {
			st, err := fi.Stat()
			if err != nil {
				t.Errorf("Error fetching stats for %s: %v", mp.Link, err)
			} else if mp.Path == "." && !st.IsDir() {
				t.Errorf("%s should be a directory, bit it isn't (mode %o)", mp.Link, st.Mode())
			} else if mp.Path != "." && !st.Mode().IsRegular() {
				t.Errorf("%s should be a regular file, but has mode %o", mp.Link, st.Mode())
			}
		}
	}
}
