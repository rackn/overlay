package overlay

import (
	"errors"
	"gitlab.com/rackn/trees/radix"
	"io"
	"io/fs"
	"path"
	"strings"
	"sync"
)

// Mount contains all the information we need to handle mounting another FS into the overlay FS.
type Mount struct {
	FS   fs.FS       // The filesystem to mount. Must be non-Nil when Link is ""
	Path string      // The path at which a filesystem should be mounted.  Must be filled.
	Link string      // The path to which all attempts to open things in this filesystem should be redirected to.
	Aux  interface{} // Auxillary mount information, for use by users of this package.
}

type Mounts = radix.Tree[string, Mount]

type mount struct {
	Mount
	owner string
}

type mounts = radix.Tree[string, *mount]

// FS provides an overlay FS.  You can mount and unmount other filesystems
// into the overlay FS, and delegate a fallback fs.FS of last resort.
type FS struct {
	sync.RWMutex
	directMounts  *mounts
	mountsByOwner *radix.Tree[string, *mounts]
	lower         *mount
}

// MountsByPrefix returns all mounted filesystems that are at or under prefix.
func (f *FS) MountsByPrefix(prefix string) (res *Mounts) {
	f.RLock()
	defer f.RUnlock()
	res = &Mounts{}
	return res.InsertWith(func(j func(string, Mount)) {
		for iter := f.directMounts.Prefix(prefix).All(); iter.Next(); {
			key, i := iter.Item()
			j(key, i.Mount)
		}
	})
}

// MountsByOwner returns all mounted filesystems by who owns them.
func (f *FS) MountsByOwner(owner string) (res *Mounts) {
	f.RLock()
	defer f.RUnlock()
	res = &Mounts{}
	return res.InsertWith(func(j func(string, Mount)) {
		if m, ok := f.mountsByOwner.Fetch(owner); ok {
			for iter := m.All(); iter.Next(); {
				key, i := iter.Item()
				j(key, i.Mount)
			}
		}
	})
	return
}

// New creates a new overlay.FS that delegates all Open lookups
// to the lower FS by default.  You may use overlay.FS directly, but it will not have
// a lower FS.
func New(lower fs.FS) *FS {
	return &FS{
		directMounts:  &mounts{},
		mountsByOwner: &radix.Tree[string, *mounts]{},
		lower:         &mount{Mount: Mount{FS: lower}},
	}
}

func (f *FS) getMount(p string) (*mount, string) {
	if f.directMounts == nil {
		f.directMounts = &mounts{}
		f.mountsByOwner = &radix.Tree[string, *mounts]{}
	}
	linksLeftToFollow := 64
	mp := p
	fn := "."
	i := len(p) - 1
	for {
		sub, ok := f.directMounts.Fetch(mp)
		if ok {
			if sub.Link == "" {
				return sub, fn
			}
			if linksLeftToFollow == 0 {
				return nil, ""
			}
			linksLeftToFollow--
			p = path.Join(sub.Link, p[i+1:])
			i = len(p) - 1
			mp = p
			fn = "."
			continue
		}
		if i == -1 {
			break
		}
		for i > 0 && p[i] != '/' {
			i--
		}
		if i == 0 {
			mp = "."
			fn = p
		} else {
			mp = p[:i]
			fn = p[i+1:]
		}
		i--
	}
	return f.lower, p
}

// MountFor fetches the Mount that is responsible for actually handling lookups
// for the passed-in path.  It returns the mountpoint and the name that should be
// used for subsequent lookups.  If there is no mount that handles p, the returned
// Mount will be the zero value.
func (f *FS) MountFor(p string) (Mount, string) {
	if !fs.ValidPath(p) {
		return Mount{}, ""
	}
	f.RLock()
	defer f.RUnlock()
	sub, name := f.getMount(p)
	return sub.Mount, name
}

// Provides fetches the FS that is responsible for actually handling lookups
// for the fs.File at p.  It returns the responsible FS, and the
// name that should be used to look p up in that FS.  If FS is nil, then
// there is no sub FS responsible for opening that file.
func (f *FS) Provides(p string) (fs.FS, string) {
	sub, name := f.MountFor(p)
	return sub.FS, name
}

// Open a file, if one exists in one of the filesystems
// we have access to.
func (f *FS) Open(p string) (fs.File, error) {
	sub, name := f.Provides(p)
	if sub == nil {
		return nil, fs.ErrInvalid
	}
	return sub.Open(name)
}

// Stat implements fs.StatFS
func (f *FS) Stat(p string) (fs.FileInfo, error) {
	sub, name := f.Provides(p)
	if sub == nil {
		return nil, fs.ErrInvalid
	}
	if st, ok := sub.(fs.StatFS); ok {
		return st.Stat(name)
	}
	fi, err := sub.Open(name)
	if err != nil {
		return nil, err
	}
	defer fi.Close()
	return fi.Stat()
}

// ReadDir implements fs.ReadDirFS
func (f *FS) ReadDir(p string) ([]fs.DirEntry, error) {
	sub, name := f.Provides(p)
	if sub == nil {
		return nil, fs.ErrInvalid
	}
	if st, ok := sub.(fs.ReadDirFS); ok {
		return st.ReadDir(name)
	}
	fi, err := sub.Open(name)
	if err != nil {
		return nil, err
	}
	defer fi.Close()
	if rd, ok := fi.(fs.ReadDirFile); ok {
		return rd.ReadDir(0)
	}
	return nil, fs.ErrInvalid
}

func (f *FS) mount(owner string, subs ...Mount) error {
	for i := range subs {
		if !fs.ValidPath(subs[i].Path) {
			return &fs.PathError{
				Op:   "MOUNT",
				Path: subs[i].Path,
				Err:  fs.ErrInvalid,
			}
		}
		if subs[i].Link != "" && !fs.ValidPath(subs[i].Link) {
			return &fs.PathError{
				Op:   "MOUNT",
				Path: subs[i].Path,
				Err:  errors.New("Invalid Link"),
			}
		}
		if (subs[i].Link == "") == (subs[i].FS == nil) {
			return &fs.PathError{
				Op:   "MOUNT",
				Path: subs[i].Path,
				Err:  errors.New("Link must be specified if FS is nil, and vice versa."),
			}
		}
	}
	if f.directMounts == nil {
		f.directMounts = &mounts{}
		f.mountsByOwner = &radix.Tree[string, *mounts]{}
	}
	var nfs *mounts
	var ok bool
	if nfs, ok = f.mountsByOwner.Fetch(owner); !ok {
		nfs = &mounts{}
	}
	for i := range subs {
		if ofs, ok := f.directMounts.Fetch(subs[i].Path); ok {
			if mbo, ok := f.mountsByOwner.Fetch(ofs.owner); ok {
				mbo, _, _ = mbo.Delete(ofs.Path)
				f.mountsByOwner = f.mountsByOwner.Insert(ofs.owner, mbo)
			}
			if cl, ok := ofs.FS.(io.Closer); ok {
				cl.Close()
			}
		}
		mnt := &mount{Mount: subs[i], owner: owner}
		f.directMounts = f.directMounts.Insert(subs[i].Path, mnt)
		nfs = nfs.Insert(subs[i].Path, mnt)
	}
	f.mountsByOwner = f.mountsByOwner.Insert(owner, nfs)
	return nil
}

// Mount mounts a series of sub filesystems.  Any previous filesystems
// mounted in those locations will be replaced.  You can use owner to
// track who mounted what fs.FS.
func (f *FS) Mount(owner string, subs ...Mount) error {
	f.Lock()
	defer f.Unlock()
	return f.mount(owner, subs...)

}

func (f *FS) unmountPaths(p ...string) {
	for i := range p {
		var ofs *mount
		var ok bool
		f.directMounts, ofs, ok = f.directMounts.Delete(p[i])
		if ok {
			if cl, ok := ofs.FS.(io.Closer); ok {
				cl.Close()
			}
			if mm, mok := f.mountsByOwner.Fetch(ofs.owner); mok {
				mm, _, _ = mm.Delete(p[i])
				f.mountsByOwner = f.mountsByOwner.Insert(ofs.owner, mm)
			}
		}
	}
}

// UnmountPaths will unmount the filesystems mounted at the following paths.
func (f *FS) UnmountPaths(p ...string) {
	f.Lock()
	defer f.Unlock()
	f.unmountPaths(p...)

}

func (f *FS) unmountOwner(owner string) {
	if m, ok := f.mountsByOwner.Fetch(owner); ok {
		f.mountsByOwner, _, _ = f.mountsByOwner.Delete(owner)
		for iter := m.All(); iter.Next(); {
			k, _ := iter.Item()
			f.directMounts, _, _ = f.directMounts.Delete(k)
		}
	}
}

// UnmountOwner will unmount all filesystems owned by owner.
func (f *FS) UnmountOwner(owner string) {
	f.Lock()
	defer f.Unlock()
	f.unmountOwner(owner)
}

// Replace will unmount all filesystems owned by owner, and then
// mount the passed-in filesystems
func (f *FS) Replace(owner string, mounts ...Mount) error {
	f.Lock()
	defer f.Unlock()
	f.unmountOwner(owner)
	return f.mount(owner, mounts...)
}

// Clear will unmount all mounted filesystems, causing all lookups
// to be handled by the lower FS.
func (f *FS) Clear() {
	f.Lock()
	defer f.Unlock()
	f.directMounts = &mounts{}
	f.mountsByOwner = &radix.Tree[string, *mounts]{}
}

// ClearAllExcept will clear all mounts except ones mounted at or under any of the prefixes.
func (f *FS) ClearAllExcept(prefixes ...string) {
	f.Lock()
	defer f.Unlock()
	newMounts := &mounts{}
	newMountsByOwner := &radix.Tree[string, *mounts]{}
	for iter := f.directMounts.All(); iter.Next(); {
		k, v := iter.Item()
		for i := range prefixes {
			if strings.HasPrefix(k, prefixes[i]) {
				newMounts = newMounts.Insert(k, v)
				m, ok := newMountsByOwner.Fetch(v.owner)
				if !ok {
					m = &mounts{}
				}
				newMountsByOwner = newMountsByOwner.Insert(v.owner, m.Insert(k, v))
				break
			}
		}
	}
	f.directMounts = newMounts
	f.mountsByOwner = newMountsByOwner
}
